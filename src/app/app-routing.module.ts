import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayersListComponent } from './components/features/players-list/players-list.component';
import { CommonModule } from '@angular/common';
import { TeamsListComponent } from './components/features/teams-list/teams-list.component';

const routes: Routes = [
  
  { path: '', redirectTo: '/teams', pathMatch: 'full' },
  { path: 'players/:id', component: PlayersListComponent },
  { path: 'teams', component: TeamsListComponent }
  
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot( routes, {useHash: true} ),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
