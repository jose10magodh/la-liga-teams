import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.scss']
})
export class TeamsListComponent implements OnInit {

  public teams:Array<any>=[];

  constructor(private teamService: ServiceService, private router: Router,) {

    this.listTeams();

  }

  ngOnInit(): void {

  }

  listTeams(){
    
    this.teams = this.teamService.getListTeams();
      console.log(this.teams);
    
  }

  idPlayer(id:any){
    this.router.navigateByUrl('players/'+id);
  }

}
