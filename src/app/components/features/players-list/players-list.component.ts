import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {

  public players:Array<any>=[];
  public idTeam:any;

  constructor(private teamService: ServiceService, private router: Router, private routeActive: ActivatedRoute,) {

    this.listPlayers();

  }

  ngOnInit(): void {
  }

  listPlayers(){
    this.routeActive.params.subscribe(data => {
      this.idTeam=data["id"];  
      this.players = this.teamService.getListPlayers();
      console.log(this.players);
      
    })

    

  }

}
